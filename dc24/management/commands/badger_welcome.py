# -*- coding: utf-8 -*-
from collections import namedtuple

from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand
from django.template import engines

from register.models import Accomm, Attendee, Food


SUBJECT = "Welcome to DebConf24! (arrival details)"

TEMPLATE = """\
Dear {{ name }},

Welcome to DebConf24, in South Korea.

Please read the contents of this mail and the Conference Venue page [1] in the
website carefully. They contain important information about your travel to
Busan, and DebConf24.

There is an abundance of very detailed information available on the wiki [2];
we aim to provide the very condensed version here.

[1] https://debconf24.debconf.org/about/venue/
[2] https://wiki.debian.org/DebConf/24/TouristGuide

== Before departing for Busan ==

Print this e-mail and the relevant information in the website (e.g. a venue
map [3]).

Check your data, and report any errors and changes (including delays or
cancellation) as soon as possible to <registration@debconf.org>:

Arriving: {{ attendee.arrival|date:"DATETIME_FORMAT" }}
Departing: {{ attendee.departure|date:"DATETIME_FORMAT" }}{% if food %}
Meals requested:{% for day in meals %}
{{ day.date|date:"SHORT_DATE_FORMAT" }}: {{ day.meals|join:", "|title }}{% endfor %}
Diet: {{ food.diet|default:"Whatever is provided" }}{% if food.special_diet %}
Details: {{ food.special_diet }}{% endif %}{% else %}
No conference-organised food requested.{% endif %}

Familiarize yourself with the Codes of Conduct for both Debian [4] and
DebConf [5]. If you are a victim of harassment, or observe it, you can contact
<community@debian.org> or members of the on-site anti-harassment team [6].

For any other kind of problem, the Front Desk and Organisation team may be of
help.

[3] https://debconf24.debconf.org/static/img/venue-map.2c11c918f581.pdf
[4] http://debconf.org/codeofconduct.shtml
[5] https://www.debian.org/code_of_conduct
[6] https://debconf24.debconf.org/about/coc/

Check your travel documents.  Will your passport remain valid for the duration
of your stay?  Do you have all necessary travel authorizations (such as a
visa)? You will need the accommodation address and contact number for your
arrival card. [7]

[7] https://debconf24.debconf.org/about/visas/

Notify your bank that you'll be travelling to South Korea to avoid having your
card(s) blocked.

Do you have travel insurance? Be sure to also check with your insurance if
you're covered for lost or stolen items, such as electronics.

Take a COVID-19 self-test, if you can get your hands on one. You wouldn't want
to infect other people at the conference or on the plane.

== Your Accommodation Details ==
{% if not accomm %}
You don't have any conference-provided accommodation, or you haven't yet paid
your invoices.  Remember to print the details of your self-organised
accommodation in case you're asked for proof by immigration officials.{% else %}
=== On-site Accommodation ===

According to our records, you're registered to stay {% if onsite %}on-site{% else %}in conference organized hotels {% endif %}
on the following days:
{% for stay in stays %}
Check-in: {{ stay.0|date }}
Checkout: {{ stay.1|date }}{% endfor %}
Room: {{ accomm.room|default:"not assigned yet" }}{% if roommates %}
Roommates: {{roommates|join:", "|default:"none"}}
(These are subject to change){% endif %}

https://debconf24.debconf.org/about/accommodation/{% endif %}

== What to Bring ==

Yourself.

A hat.  It's very sunny and hot in Busan, and a hat (preferably broad-brimmed)
is essential.

Mosquito spray if you're sensitive.

Printed PGP/GPG fingerprint slips, if you want to have your key signed by your
peers. There will be an organized continuous keysigning for attendees with
pre-registered fingerprints.

Something to contribute to the Cheese and Wine party! Cheese should be declared
during customs clearance [8] upon arrival.

You are allowed to bring up to 5kg of cheese, which must be pasteurised and
comply with the following requirements:

Factory-made cheese should be factory-sealed: vacuum-packed and labelled.
Handmade cheese should be accompanied by a “Sterilization Certificate” issued
by the manufacturer.

With tax-exemption, you can bring liquor up to 1L and up to 400 USD in value
[9]. Additional quantities can be brought with a customs declaration, subject
to customs duty payment.

[8] https://www.customs.go.kr/english/cm/cntnts/cntntsView.do?mi=8069&cntntsId=2743
[9] https://www.customs.go.kr/english/cm/cntnts/cntntsView.do?mi=8028&cntntsId=2716

== Getting to Busan ==

If you are arriving at ICN (Seoul Incheon airport) and plan to take the Train
To Busan, follow signs for the Airport Railroad. The direct train from the
airport to Seoul station goes approximately every 40 minutes, and the fare is
KRW 11,000. It's a good idea to have some cash or a T-Money/Cashbee card for
this, as the machine doesn't always accept foreign credit cards.

From Seoul station, you can take the Train To Busan! You can choose the faster
KTX train, which takes about 2.5 hours to reach Busan, or a slower train, which
takes anywhere from 4 to 6 hours and is less expensive.  The fare for the KTX
train is KRW 59,000.

Head downstairs to exit 3 of the station. The bus stop is on a traffic island
on the main road in front of you. The bus stop is on your left, the bus will
head to your right.  Take bus 27 for 18 stops to Pukyong National Univ. Daeyeon
Campus. The ride will be half an hour.

Cross the road to your left at the traffic island, you'll cross twice.  The
building (marked 13) is about 50m further down the road on your left.

To pay the bus fare, you will need either a transport card (T-Money or
Cashbee), or cash.  If you are using a transport card, remember to tap in when
boarding. To transfer busses, tap out before leaving the bus. If you are paying
by cash, no further action is required.

=== Taxis ===

The address of the venue, to show the driver or paste into the taxi-hailing app:
- 목적지: 부경대학교 대연캠퍼스 공학1관 <=> Destination: Pukyong National
  University Daeyeon Campus Engineering 1 Building.

Show the driver the following directions to reach the venue, as the entire
campus has the same street address:
- 기사님, 부경대학교 대연캠퍼스 정문으로 들어오셔서 바로 우회전 해주세요.
  그리고 직진하시면 주차장 말고 좌회전 하시면 오른쪽에 이마트24 편의점이
  있습니다. 거기서 내려주세요.

The fare from Busan station to the venue will be ~13,000 KRW.

{% if accomm and not onsite %}The address of the accommodation:

    - 목적지: 웰킨 호텔 <=> Destination: Welkin Hotel

    - 주소: 부산광역시 남구 유엔평화로4번길 30 <=> Address: 30 UN pyeonghwa-ro
      4beon-gil, Nam-gu, Busan{% endif %}

If someone approaches you offering a taxi ride, politely decline. Take
legitimate taxis, which can be recognised by the sign on top [10], from one of
the taxi ranks outside the station. Uber is also available, but functions as a
taxi-hailing app in Busan.

[10] https://people.debian.org/~cwryu/dc24-misc/Taxi01.png

== Busan Airport Arrival ==

If you are arriving at Gimhae International Airport (PUS), you can get to
{% if onsite %}the venue{% else %}the hotel{% endif %} using the Busan Metro system
from the airport. Get on at Gimhae International Airport station and head
towards Sasang, transfer to metro line 2 station of Sasang. From there, board a
train heading towards Jangsan, and get off at {% if onsite or not accomm %}"Kyungsung Univ. Pukyong National Univ"{% else %}"Daeyeon"{% endif %}.

== Laundry ==

Laundry facilities are available at a laundrette near the venue.  Further
details will be provided in an announcement mail during DebCamp.

== Front Desk location ==

Frontdesk info: Front Desk is located in Engineering Building 1, on the 2nd
floor.

The front desk phone number is +82 10-9429-1993.
We have Signal installed, text messages are preferred.

Save this in your phone now :)

Have a safe trip. We look forward to seeing you in Busan!

The DebConf Team
"""

Stay = namedtuple('Stay', ('checkin', 'checkout'))
Meal = namedtuple('Meal', ('date', 'meals'))


def meal_sort(meal):
    return ['breakfast', 'lunch', 'dinner'].index(meal)


class Command(BaseCommand):
    help = 'Send welcome emails'

    def add_arguments(self, parser):
        parser.add_argument('--yes', action='store_true',
                            help='Actually do something'),
        parser.add_argument('--username',
                            help='Send mail to a specific user, only'),

    def badger(self, attendee, dry_run):
        name = attendee.user.userprofile.display_name()
        to = attendee.user.email

        try:
            accomm = attendee.accomm
            roommates = accomm.get_roommates()
            if roommates:
                roommates = [
                    '%s (%s)' % (attendee.user.userprofile.display_name(), attendee.user.username)
                    for attendee
                    in roommates
                ]
        except Accomm.DoesNotExist:
            accomm = None
            roommates = None

        if attendee.billable() and not attendee.paid():
            accomm = None
            roommates = None

        try:
            food = attendee.food
        except Food.DoesNotExist:
            food = None

        if accomm:
            stays = list(attendee.accomm.get_stay_details())
            onsite = attendee.accomm.room.startswith(('M', 'F', 'GH'))
        else:
            stays = None
            onsite = False

        meals = []
        if food:
            by_day = {}
            for meal in food.meals.all():
                by_day.setdefault(meal.date, []).append(meal.meal)
            for date, days_meals in sorted(by_day.items()):
                meals.append(Meal(date, sorted(days_meals, key=meal_sort)))

        ctx = {
            'accomm': accomm,
            'roommates': roommates,
            'attendee': attendee,
            'food': food,
            'meals': meals,
            'name': name,
            'stays': stays,
            'onsite': onsite,
        }

        template = engines['django'].from_string(TEMPLATE)
        body = template.render(ctx)

        if dry_run:
            print('Would badger %s <%s>' % (name, to))
            return

        email_message = EmailMultiAlternatives(
            SUBJECT, body, to=["%s <%s>" % (name.replace(',', ''), to)])
        email_message.send()

    def handle(self, *args, **options):
        dry_run = not options['yes']
        if dry_run:
            print('Not actually doing anything without --yes')

        queryset = Attendee.objects.all()
        if options['username']:
            queryset = queryset.filter(user__username=options['username'])

        for attendee in queryset:
            if attendee.user.userprofile.is_registered():
                self.badger(attendee, dry_run)
