# -*- coding: utf-8 -*-
from csv import DictWriter

from django.conf import settings
from django.core.management.base import BaseCommand

from debconf.tz import aoe_datetime
from register.models import QueueSlot


class Command(BaseCommand):
    help = 'Export shipping addresses as CSV'

    def handle(self, *args, **options):
        fields = ('username', 'name', 'email', 't_shirt_size',
                  'contact', 'company', 'line1', 'line2', 'city', 'province',
                  'postal_code', 'country', 'phone_number', 'notes')
        writer = DictWriter(self.stdout, fields)
        writer.writeheader()

        queue_slots = QueueSlot.objects.filter(
            queue__name='Swag',
            timestamp__lt=aoe_datetime(settings.DEBCONF_CONFIRMATION_DEADLINE),
            attendee__completed_register_steps=8,
        ).exclude(
            attendee__t_shirt_size='',
        )

        for queue_slot in queue_slots:
            attendee = queue_slot.attendee
            user = attendee.user
            userprofile = user.userprofile
            address = attendee.shipping_address
            writer.writerow({
                'username': user.username,
                'name': userprofile.display_name(),
                'email': user.email,
                't_shirt_size': attendee.t_shirt_size,
                'contact': address.contact,
                'company': address.company,
                'line1': address.line1,
                'line2': address.line2,
                'city': address.city,
                'province': address.province,
                'postal_code': address.postal_code,
                'country': address.country,
                'phone_number': address.phone_number,
                'notes': attendee.notes,
            })
