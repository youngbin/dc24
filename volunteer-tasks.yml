debcamp_days: &debcamp_days
  - 2024-07-21
  - 2024-07-22
  - 2024-07-23
  - 2024-07-24
  - 2024-07-25
  - 2024-07-26
  - 2024-07-27

debconf_days: &debconf_days
  - 2024-07-28
  - 2024-07-29
  - 2024-07-30
  - 2024-07-31
  - 2024-08-01
  - 2024-08-02
  - 2024-08-03
  - 2024-08-04

all_days: &all_days
  - 2024-07-21
  - 2024-07-22
  - 2024-07-23
  - 2024-07-24
  - 2024-07-25
  - 2024-07-26
  - 2024-07-27
  - 2024-07-28
  - 2024-07-29
  - 2024-07-30
  - 2024-07-31
  - 2024-08-01
  - 2024-08-02
  - 2024-08-03
  - 2024-08-04

on_call_slots: &on_call_slots
  - start: '10:00'
    end: '12:00'
  - start: '14:30'
    end: '16:30'
  - start: '17:00'
    end: '18:00'

tasks:

  - category: Video
    name: Talk Meister
    description: >
      The Talk Meister introduces the speaker, keeps track of the time and
      coordinates the question and answer sessions, if there is one. At the end
      of the talk they also thank the speaker.

      For BoFs, the Talk Meister role is assumed by the person who leads the
      BoF.

      Detailed documentation on the Talk Meister's tasks can be found in the
      Talk Meister's guide.
    nbr_volunteers_min: 1
    nbr_volunteers_max: 1
    required_permission: volunteers.accept_video_tasks
    video_task: true

  - category: Video
    name: Room Coordinator
    description: >
      The room coordinator is responsible for ensuring that all equipment is
      working, and that all necessary volunteers are present, set up, and know
      how to do their role. If someone does not arrive in time, then the
      coordinator must find someone to replace them, or take their place.
    nbr_volunteers_min: 1
    nbr_volunteers_max: 1
    required_permission: volunteers.accept_video_tasks
    video_task: true

  - category: Video
    name: Director
    description: >
      The Director controls what is recorded and what goes out onto the stream.
      They decide what video sources should be shown on stream. They are also
      responsible for monitoring the stream for quality assurance.

      Detailed documentation on the Director's tasks can be found in the
      Director's guide.
    nbr_volunteers_min: 1
    nbr_volunteers_max: 1
    required_permission: volunteers.accept_video_tasks
    video_task: true

  - category: Video
    name: Camera operator
    description: >
      There are two cameras in the room, each operated by one team member.
      Generally speaking, one camera is meant to point at the speaker, and the
      other at the audience. However, these allocations can change if needed.

      For example, during the talks, the audience camera can be used as a
      second shot of the speaker. The speaker camera can also be pointed to the
      audience, if there is a discussion with many people distributed around
      the room, for instance.

      Detailed documentation on the Camera operator's tasks can be found in the
      Camera operator's guide.
    nbr_volunteers_min: 2
    nbr_volunteers_max: 2
    required_permission: volunteers.accept_video_tasks
    video_task: true

  - category: Video
    name: Sound technician
    description: >
      The sound technician operates the audio mixer and is responsible for
      ensuring that audio is clear and intelligible on the stream and
      recordings as well as in the room itself. The sound technician must know
      how to operate the audio devices and mixer, as this role is critical for
      not only the video streaming and recording, but also for the talk itself.

      Detailed documentation on the Sound technician's tasks can be found in
      the Sound technician's guide.
    nbr_volunteers_min: 1
    nbr_volunteers_max: 1
    required_permission: volunteers.accept_video_tasks
    video_task: true

  - category: Video
    name: Videoteam member on call
    description: >
      For all sessions, there is a core videoteam member who can be reached
      quickly to help solve problems with the infrastructure.

      If this person is not in the talk room, they can reached on
      #debconf-video.
    nbr_volunteers_min: 1
    nbr_volunteers_max: 1
    days:
      - 2024-07-28
      - 2024-07-29
      - 2024-07-30
      - 2024-07-31
      - 2024-08-01
      - 2024-08-02
      - 2024-08-03
      - 2024-08-04
    hours: *on_call_slots
    required_permission: volunteers.accept_video_tasks

  - category: Front desk
    venues:
      - Front desk
    name: Front desk
    description: |
      You will help the Front desk team in the registration process and
      preparing swag.
      If you haven't done this task at DebConf before, talk to Front desk staff
      before signing up.
    nbr_volunteers_min: 1
    nbr_volunteers_max: 1
    days: *all_days
    hours: *on_call_slots

  - category: Cleanup
    name: Cleaning of the hacklabs
    description: |
      Clean up any trash, and generally tidy the hacklabs, so they are nice and
      clean for the next day.
    days: *all_days
    hours:
      - start: '18:00'
        end: '19:00'
    nbr_volunteers_min: 1
    nbr_volunteers_max: 2

  - category: Catering
    name: Meal Setup
    venues:
      - Dining Hall (Mirae 4F)
    description: |
      Help attendees to get fed. Arrive before the start of the meal, to help
      setup. Check that things are labelled, and the vegans will be well fed.
    days: *all_days
    hours:
      - start: '07:30'
        end: '8:00'
      - start: '11:30'
        end: '12:00'
      - start: '17:30'
        end: '18:00'
    nbr_volunteers_min: 2
    nbr_volunteers_max: 2

  - category: Catering
    name: Food Hero
    venues:
      - Dining Hall (Mirae 4F)
    description: |
      Help attendees to get fed.
      Monitor the meal service. Get the caterers to re-stock food and cutlery,
      as necessary. Guide attendees to clean up after themselves (food only, in
      the round trash bin).
    days: *all_days
    hours:
      - start: '08:00'
        end: '10:30'
      - start: '12:00'
        end: '14:00'
      - start: '18:00'
        end: '20:00'
    nbr_volunteers_min: 2
    nbr_volunteers_max: 2
