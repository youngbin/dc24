---
name: 자원 봉사
---

# 자원 봉사

2024 데비안 컨퍼런스(DebConf24)는 100% 자원 봉사로 진행되는 행사입니다.

행사가 진행되고 필요한 작업이 생기면 수시로 [자원 봉사(Volunteer)](/volunteers/) 페이지가 업데이트 됩니다.

컨퍼런스에 [참가(Register)](/register/) 하신 분들은,  [자원 봉사(Volunteer)](/volunteers/) 페이지에서, 원하는 시간(slot)을 지정하여 선착순으로 원하는 작업(Task)를 지원하실 수 있습니다.

자원 봉사에 지원해 주신 분들은 소정의 기념품이 지급됩니다. (물품의 재고에 따라 달라질 수 있습니다.) 자원 봉사에 지원해주세요! :)


## 비디오 팀 자원 봉사

혜택 : Video team 티셔츠

* Camera Operator (세션 당 2인)
* Room Coordinator (세션 당 1인)
* Director (세션 당 1인)
* Sound Technician (세션 당 1인)
* Talkmeister (세션 당 1인)

비디오팀 자원 봉사는 사전 트레이닝을 (1-2시간 내외) 이수해야 합니다. 컨퍼런스 중 트레이닝이 몇차례 있으니 일정을 미리 확인하세요.

상세 : https://debconf-video-team.pages.debian.net/docs/volunteer_roles.html


## 일반 자원 봉사

혜택 : Volunteer 티셔츠

* Bar (시간 당 1~2인, 미정)
* Catering (시간 당 2~3인, 미정)
* (미정)
