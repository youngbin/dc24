---
name: Call for Proposals
---

# Call for Proposals

<!--
To: debian-devel-announce@lists.debian.org, debconf-announce@lists.debian.org
Subject: Call for Proposals: DebConf24, South Korea
-->

The DebConf Content team would like to call for proposals for the DebConf24
conference, which will take place in Busan, South Korea, from Sunday July 28th
to Sunday August 4th 2024. It will be preceded by DebCamp from Sunday July
21st to Saturday July 27th 2024.

## Submitting an Event

You can now submit an [event proposal]. Events are not limited to
traditional presentations or informal sessions (BoFs): we welcome
submissions of tutorials, performances, art installations, debates, or any
other format of event that you think would be of interest to the Debian
community.

Regular sessions may either be 20 or 45 minutes long (including time for
questions), other kinds of sessions (workshops, demos, lightning talks, ...)
could have different durations. Please choose the most suitable duration for
your event and explain any special requests.

In order to submit a talk, you will need to create an account on the site.
We suggest that Debian Salsa account holders (including DDs and DMs) use
their [Salsa] login when creating an account. However, this isn't required,
as you can sign up with an e-mail address and password.

[event proposal]: https://debconf24.debconf.org/talks/new/
[Salsa]: https://salsa.debian.org/

## Timeline

* If you need to apply for a bursary, submit your proposal by May 8th.
* Early submission deadline: Sunday, June 2
* Acceptance notification: Sunday, June 23
* Final submission deadline: Sunday, July 14

Talks submitted before the early submission deadline are *guaranteed* to be
accepted or rejected by the acceptance notification date. Talks submitted
after the early submission deadline, but before the final submission deadline,
will be handled on a best effort basis, and if there is still space on the
schedule and they are accepted, are guaranteed to have video coverage.

We might send acceptance notification in batches as submissions come in, even
before the final acceptance notification day. So the earlier you submit your
proposal, the earlier you might get notified about it being accepted.

After the final submission deadline, we will still accept event proposals for
ad-hoc sessions (self-organized sessions), and we cannot guarantee video
coverage.

## Topics and Tracks

Though we invite proposals on any Debian or FLOSS related subject, we have
some broad topics on which we encourage people to submit proposals,
including but not limited to:

- Introduction to Free Software & Debian
- Packaging, policy, and Debian infrastructure
- Systems administration, automation and orchestration
- Cloud and containers
- Security
- Community, diversity, local outreach and social context
- Internationalization, Localization and Accessibility
- Embedded & Kernel
- Debian in Arts & Science
- Debian Blends and Debian derived distributions

## Talk proposal help on IRC

Nevertheless, it is worth to note that we give priority to Debian-related
proposals. If your event addresses a broader theme, make sure the link to
Debian is evident, or explicitly state how the Debian community would benefit
from being exposed to it.

If you want to submit a talk proposal but would like to discuss it first, feel
free to pop by the #debconf-content IRC channel on OFTC, where the DebConf
content team will be available to help potential speakers prepare their talk
proposals for DebConf.

Please make sure you stick around the channel if you don't get an immediate
response, because it could be coming from the other side of the world.

## Outreach

We actively encourage proposals from individuals who are traditionally
underrepresented in Free Software and technology in general, especially within
the Debian community. We kindly ask for your help in reaching out to people
who might not be listening to channels that typically circulate our call.
Perhaps they are listening but need a bit of encouragement to see themselves
as potential presenters! This includes, but is not limited to, people of
color, women, members of the LGBTQ+ community, gender non conforming and
transgender individuals, people with disabilities, indigenous peoples,
individuals of varying socio-economic backgrounds, those with differing levels
of formal education, individuals from various global communities, and other
marginalized populations. Please help us spread the word!

## Code of Conduct

Our event is covered by a [Code of Conduct] designed to ensure everyone’s
safety and comfort. The code applies to all attendees, including speakers
and the content of their presentations. Do not hesitate to contact us at
<content@debconf.org> if you have any questions or are unsure about certain
content you’d like to present.

[Code of Conduct]: https://debconf.org/codeofconduct.shtml

## Video Coverage

Providing video is one of the [conference goals], as it makes the content
accessible to a wider audience. Unless speakers opt-out, scheduled talks may be
streamed live over the Internet to promote remote participation, and recordings
will be published later under the [DebConf license] (MIT/Expat), as well as
presentation slides and papers whenever available.

[conference goals]: https://debconf.org/goals.shtml
[DebConf license]:  https://meetings-archive.debian.net/pub/debian-meetings/LICENSE

## Closing note

DebConf24 is accepting sponsors; if you are interested, or think you know
of others who would be willing to help, please get in touch with
<sponsors@debconf.org>.

In case of any questions, or if you wanted to bounce some ideas off us
first, please do not hesitate to reach out to the content team at
<content@debconf.org>.

We hope to see you in Busan!

The DebConf team
